//must specify current path
//this is done to ensure a standalone package dependancies and tests
//are found by requirejs
this.$PROJECT_NAME = 'curves';

/**
 * Spec must be wrapped in a define function
 * then be followed by a window.onload() to trigger jasmine tests
 * window.onload() is a hack around jasmine not being triggered when using requirejs
 * first args array is the specs to be executed
 */
define([
    'jasmine-boot',
    'p!-logger',
    './bezierDrawSpec',
    './bezierSpec'
], function(
    $jasmine,
    $logger
) {
    'use strict';
    window.onload();
});

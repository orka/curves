define(['lib/bezier'], function($Bezier) {
    'use strict';
    var bez;
    var width = 1300;
    var height = 550;
    var offset = 20;
    var ctx;
    var path = [];
    var count = 0;
    var colors = ['green', 'blue', 'orange', 'black', 'purple', 'turquoise', 'brown', 'black', 'pink', 'red', 'blueviolet', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate', 'coral', 'crimson', 'cyan', 'darkmagenta', 'deeppink', 'firebrick', 'fuchsia', 'maroon'];
    var int;
    var alpha;
    var duration;
    var canvas = document.createElement('Canvas');
    canvas.width = width + offset * 2;
    canvas.height = height + offset * 2 + 40;
    document.body.appendChild(canvas);
    ctx = canvas.getContext('2d');
    ctx.font = '12px Arial';
    ctx.textBaseline = 'alphabetic';
    ctx.lineWidth = 2;

    startNew();

    function startNew() {
        var _i, _points = [];
        var _count = Math.floor(Math.random() * 50);
        for (_i = 0; _i < _count; _i += 1) {
            _points.push([Math.random(), Math.random()]);
        }
        path = [];
        int = setInterval(update, 10);
        count = 0;
        duration = 5000 + Math.round(Math.random() * 20000);
        //duration = 5000;
        bez = $Bezier(_points, duration, true);
        alpha = null;
        bez.addListener('complete', function() {
            alpha = 0.3;
            // draw ();
            clearInterval(int);
            setTimeout(startNew, 2000);
        });
        colors = colors.sort(function() {
            return 0.5 - Math.random();
        });
    }



    function update() {
        bez.update();
        draw();
    }

    function draw() {
        var _points;
        _points = [bez.__points__].concat(bez.__debugPoints);

        ctx.globalAlpha = 1;
        //draw bg
        fillBg();
        //draw all of the debug controll points
        //these are normally not kept unless debug flag is on
        _points.forEach(function(__pointsArray, __i) {
            ctx.globalAlpha = alpha || 0.2 + 0.2 * (__i / _points.length);
            ctx.fillStyle = ctx.strokeStyle = __i === _points.length - 1 ? 'red' : colors[__i % colors.length];
            drawPoints(__pointsArray);
            drawLines(__pointsArray);
        });
        //reset alpha
        ctx.globalAlpha = 1;
        //collect all the path point
        //we'll use this to draw the main path
        if (count % 2 === 0) {
            path.push(bez.point);
        }

        // count += 1;
        // ctx.fillStyle = ctx.strokeStyle = 'red';
        ctx.beginPath();
        //draw final path
        ctx.moveTo(offset, offset + height);
        //all the path point can be drawn - a lot of them!!!
        path.forEach(function(__point) {
            ctx.lineTo(offset + __point[0] * width, offset + height - height * __point[1]);
        });
        ctx.lineWidth = 4; //fat
        ctx.stroke();

        ctx.globalAlpha = 0.8;

        //draw axys
        //time
        ctx.fillStyle = 'grey';
        ctx.fillText('time', width, height + offset);
        ctx.fillRect(offset, height + offset, width, 1);
        //progresstion
        ctx.save();
        ctx.translate(0, canvas.height);
        ctx.rotate(-Math.PI / 2);
        ctx.fillText('progression', height, offset);
        ctx.fillRect(canvas.width - width + offset * 1.5, offset, width, 1);
        ctx.restore();

        //duration
        ctx.fillText('duration: ' + bez.getDuration() + 'ms', offset * 2, offset);
        ctx.fillText('lapsed time: ' + (bez.getDuration() * bez.getProgress()).toFixed(2) + 'ms', width / 3, offset);
        //points count
        ctx.fillText('controls: ' + _points.length, width /2, offset);

        ctx.fillStyle = 'grey';
        ctx.fillText('progress:' + bez.getProgress().toFixed(4), offset, canvas.height - 15);
        ctx.fillRect(offset, canvas.height - 15, bez.getProgress() * width, 3);
        ctx.fillRect(offset, 0, bez.getProgress() * width, 3);

        //draw progresion
        ctx.fillStyle = 'green';
        ctx.fillText('dialation progression: ' + bez.getLerp().toFixed(4), offset, canvas.height - 45);
        ctx.fillRect(offset, canvas.height - 45, bez.getLerp() * width, 3);
        ctx.fillRect(offset, 0, bez.getLerp() * width, 3);
        ctx.fillRect(0, bez.getLerp() * height + offset * 2, 10, 10);

        ctx.fillStyle = 'purple';
        ctx.fillText('dialation time: ' + bez.getLerp(true).toFixed(4), offset, canvas.height - 30);
        ctx.fillRect(offset, canvas.height - 30, bez.getLerp(true) * width, 3);
        ctx.fillRect(offset, 0, bez.getLerp(true) * width, 3);
        ctx.fillRect(width + offset * 2 - 10, bez.getLerp(true) * height + offset * 2, 10, 10);
    }

    function fillBg() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = 'rgba(240,255,240, 1)';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    function drawPoints(__array) {
        var _i, _point;
        for (_i = 0; _i < __array.length; _i += 1) {
            _point = __array[_i];
            drawPoint(offset + _point[0] * width, offset + height - _point[1] * height, 5);
        }
    }

    function drawPoint(__x, __y, __rad) {
        var startAngle = 0.0 * Math.PI;
        var endAngle = 2.0 * Math.PI;
        ctx.beginPath();
        ctx.arc(__x, __y, __rad, startAngle, endAngle);
        ctx.fill();
    }

    function drawLines(__array) {
        var _i;
        for (_i = 0; _i < __array.length - 1; _i += 1) {
            drawLine(__array[_i], __array[_i + 1], 1);
        }
    }

    function drawLine(__pointA, __pointB, __width) {
        ctx.beginPath();
        ctx.moveTo(offset + __pointA[0] * width, offset + height - height * __pointA[1]);
        ctx.lineTo(offset + __pointB[0] * width, offset + height - height * __pointB[1]);
        ctx.lineWidth = __width;
        ctx.stroke();
    }
});

define(['lib/bezier'], function($bezier) {
    'use strict';


    describe('', function() {
        var DURATION = 200;
        var instance;

        var listeners;


        beforeEach(function(done) {
            instance = $bezier(null, DURATION);

            listeners = {
                start: function() {},
                end: function() {},
                complete: function() {}
            };

            //
            spyOn(listeners, 'start');
            spyOn(listeners, 'end');
            spyOn(listeners, 'complete');
            //
            instance.addListener('start', listeners.start);
            instance.addListener('end', listeners.end);
            instance.addListener('complete', listeners.complete);

            setTimeout(done, DURATION / 2);
        });

        it('Should not emit "start", "end" and "complete" events before first update was called', function(done) {
            expect(listeners.start).not.toHaveBeenCalled();
            expect(listeners.end).not.toHaveBeenCalled();
            expect(listeners.complete).not.toHaveBeenCalled();
            done();
        });

        it('Should emit "start" but not the "end" and "complete" events before curve reaches its end', function(done) {

            instance.update();
            expect(listeners.start).toHaveBeenCalled();
            expect(listeners.end).not.toHaveBeenCalled();
            expect(listeners.complete).not.toHaveBeenCalled();
            done();
        });
    });

    describe('', function() {
        var DURATION = 100;
        var instance;

        var listeners;

        beforeEach(function(done) {
            instance = $bezier(null, DURATION);

            listeners = {
                start: function() {},
                end: function() {},
                complete: function() {}
            };
            //
            spyOn(listeners, 'start');
            spyOn(listeners, 'end');
            spyOn(listeners, 'complete');
            //
            instance.addListener('start', listeners.start);
            instance.addListener('end', listeners.end);
            instance.addListener('complete', listeners.complete);

            setTimeout(done, DURATION * 2);
        });

        it('Should emit "start" and "end" but not "complete" events after curve reaches its end and update was called ones', function(done) {
            instance.update();
            expect(listeners.start).toHaveBeenCalled();
            expect(listeners.end).toHaveBeenCalled();
            expect(listeners.complete).not.toHaveBeenCalled();
            done();
        });

        it('Should emit "start" and "end" and "complete" events after curve reaches its end and update was called twice', function(done) {
            instance.update();
            instance.update();
            expect(listeners.start).toHaveBeenCalled();
            expect(listeners.end).toHaveBeenCalled();
            expect(listeners.complete).toHaveBeenCalled();
            done();
        });
    });
});

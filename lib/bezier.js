define(['p!-eventful',
    'p!-math',
    'p!-assertful',
    'p!-curves/error'
], function($eventful,
    $math,
    $assertful,
    $error) {
    'use strict'; //always run in strict mode
    /**
     * Standard control point 0
     *
     * @type       {number}
     */
    var edge0 = 0.0;
    /**
     * Standard control point 1
     *
     * @type       {number}
     */
    var edge1 = 1.0;
    /**
     * Pre-built arrays to be used assuming non modified state
     * @type {Float32Array}
     */
    var head32 = new Float32Array([edge0, edge0]);
    /**
     * Pre-built arrays to be used assuming non modified state
     * @type {Float32Array}
     */
    var tail32 = new Float32Array([edge1, edge1]);
    /**
     * Default duration of one second
     *
     * @type       {number}
     */
    var duration = 1000;
    /**
     * Class
     * @class      Bezier Bezier
     * @type       {Class}
     */
    var Bezier;

    /*
    point [time, lerp]
    | lerp
    |
    |
    |
    |
    |
    ---------------------
                    time
     */
    /**
     * Interpolate values of A, B points in time 0-1
     * @param   {Array} __pointA time, lerped time values
     * @param   {Array} __pointB time, lerped time values
     * @param   {Number} __t      progress to interpolate
     * @returns {Array}          interpolated value of time, lerped time
     */
    function lerp(__pointA, __pointB, __t) {
        //extract values to prevent unnecessary Array access operations
        var _a1 = __pointA[0],
            _a2 = __pointA[1],
            _b1 = __pointB[0],
            _b2 = __pointB[1];
        return new Float32Array([(_b1 - _a1) * __t + _a1, (_b2 - _a2) * __t + _a2]);
    }

    /**
     * maps array of floats to Float32Array
     * @param   {Array} __array shallow array to be transformed into Float32Array
     * @returns {Float32Array}  new Float32Array
     */
    function mapTo32(__array) {
        return new Float32Array(__array);
    }

    /**
     * Point array processing involves reduction of two points to one time, process values point or a position
     * Function will recurse till there is only one point remaining representing time, lerped time values
     * @param   {Array} __points   all points array (including edge)
     * @param   {Float} __progress 0-1 progress value by which to interpolate the points
     * @returns {Array}            time, lerped time value [#,#]
     */
    function processPoints(__points, __progress) {
        var _i, _positions = [];

        //calculate all middle points relations
        for (_i = 0; _i < __points.length - 1; _i += 1) {
            //calculate the position in time, lerped time for each middle point
            //collect the result
            _positions.push(lerp(__points[_i], __points[_i + 1], __progress));
        }

        //if we have more than 1 positions - scan again
        if (_positions.length > 1) {
            return this.__processPoints(_positions, __progress);
        }
        //return a final computed point
        return _positions[0];
    }

    /**
     * Point array processing involves reduction of two points to one time, process values point or a position
     * Function will recurse till there is only one point remaining representing time, lerped time values
     * + storage of debug points
     * @param   {Array} __points   all points array (including edge)
     * @param   {Float} __progress 0-1 progress value by which to interpolate the points
     * @returns {Array}            time, lerped time value [#,#]
     */
    function processPointsDebug(__points, __progress) {
        var _i, _positions = [];

        //calculate all middle points relations
        for (_i = 0; _i < __points.length - 1; _i += 1) {
            //calculate the position in time, lerped time for each middle point
            //collect the result
            _positions.push(lerp(__points[_i], __points[_i + 1], __progress));
        }
        //useful only for debug render of all control points
        if (this.__debugPoints != null) {
            this.__debugPoints.push(_positions);
        }

        //if we have more than 1 positions - scan again
        if (_positions.length > 1) {
            return this.__processPoints(_positions, __progress);
        }
        //return a final computed point
        return _positions[0];
    }


    /**
     * Reduces points to one position time, lerped time that will be the current position point
     * @param   {Number} __progress 0-1 progression of curve
     */
    function curve(__progress) {
        this.__progress__ = __progress;
        //cap near time value (not lerp!)

        //call started regardless of the near start edge proximity
        if (!this.started) {
            this.started = true;
            this.emit('start');
        }

        if ($math.nearmore(__progress, 1.0)) {
            this.onEnded();
        } else {
            //position will reflect the time/progress coordinates of the point calculus
            //it may be the the position progress is below or above the progress - depending on points coordinates
            this.point = this.__processPoints(this.__points__, __progress);
        }
    }

    /**
     * Reduces points to one position time, lerped time that will be the current position point
     * @param   {Number} __progress 0-1 progression of curve
     */
    function curveDebug(__progress) {
        this.__progress__ = __progress;

        //only useful for a debug graphics
        // see specs/
        if (this.__debugPoints) {
            //reset array
            this.__debugPoints.length = 0;
        }

        //call started regardless of the near start edge proximity
        if (!this.started) {
            this.started = true;
            this.emit('start');
        }

        if ($math.nearmore(__progress, 1.0)) {
            this.onEnded();
        } else {
            //position will reflect the time/progress coordinates of the point calculus
            //it may be the the position progress is below or above the progress - depending on points coordinates
            this.point = this.__processPoints(this.__points__, __progress);
        }
    }



    /**
     * @class Bezier processor of time, lerped time values by a duration in ms
     * as a subclass produces 'ended' event
     * @type {Bezier}
     */
    Bezier = {
        /**
         * @constructor
         * @param   {Array} __points   collection of control points only - no limit
         * @param   {Number} __duration time in MS of a duration of the curve
         * @param {Boolean} __debug if true debug points will be collected this.__debugPoints
         */
        constructor: function(__points, __duration, __debug) {
            //validate/sanitize input
            if (!$assertful.optArray(__points)) {
                $error($error.TYPE.TYPE_ERROR, 'Please specify points as "Array" or null|undefined to create a linear curve').throw();
            }

            if (!$assertful.optPint(__duration)) {
                $error($error.TYPE.DURATION, 'Duration - if specified must only be a positive number').throw();
            }

            //initialize events
            Bezier.super.constructor.call(this);

            if (__points) {
                //concatenate edge around points
                this.__points__ = [
                    head32
                ].concat(__points.map(mapTo32)).concat(
                    tail32
                );
            } else {
                this.__points__ = [
                    head32, //time, lerped time
                    tail32 //time, lerped time
                ]; //[[#,#], ...]
            }
            //set duration
            this.__duration__ = __duration || duration;
            //start point in case value is retrieved before the update is called
            this.point = head32.slice(0);
            //set start time
            this.__created__ = Date.now();
            //progress
            this.__progress__ = 0;

            //dynamic assignment of method to reduce overhead of debug code
            if (__debug) {
                this.__debugPoints = [];
                this.__processPoints = processPointsDebug;
                this.__curve = curveDebug;
            } else {
                this.__processPoints = processPoints;
                this.__curve = curve;
            }
        },

        /**
         * de-constructs self and super
         */
        deconstructor: function() {
            Bezier.super.deconstructor.call(this);
            this.__points__ = null;
            this.__duration__ = null;
            this.__created__ = null;
            this.__debugPoints = null;
            this.point = null;
            this.__created__ = null;
            this.__progress__ = null;
        },

        /**
         * resets start time and updates current point
         * "started" event is emitted
         */
        start: function() {
            this.__created__ = Date.now();
            this.started = false;
        },

        /**
         * Sets point to end values
         * "ended" event is emitted
         */
        end: function() {
            // this.__progress__ = 1.0;
            this.__created__ = Date.now() - this.__duration__;
        },

        /**
         * Has two functions:
         * 1) to emit "end" event when called the first time due to proximity or passage of edge
         * 2) to emit "complete" event ones the "end" event was called and same conditions encountered.
         */
        onEnded: function() {
            this.__progress__ = 1.0;
            if (!this.complete) {
                this.complete = true;
                //do a final run
                // this.__processPoints(this.__points__, this.__progress__);
                //manually set point
                this.point = tail32.slice(0);
                this.emit('end');
            } else {
                this.emit('complete');
            }
        },

        /**
         * Lerps points over time (now)
         * Starts progress if not started
         */
        update: function() {
            this.__curve((Date.now() - this.__created__) / this.__duration__);
        },

        /**
         * Assign progression position between 0.0-1.0
         * to then compute the point of progression assigned - based on control points
         * - Note: if not value supplied - curve is set to a start point of progression - 0
         * @param   {Number} __progress Opt - override progress
         */
        set: function(__progress) {
            if ($assertful.number(__progress)) {
                this.__created__ = this.__created__ + this.__duration__ * __progress;
            } else if (!this.__created__) {
                this.start();
            }
        },

        /**
         * Returns current time point value
         * If a values was provided - the curve point returned - will be based (computed) on the value of progress provided
         * @param {Number} __progress a progress value
         * @returns {Number} progression dilation value
         */
        get: function(__progress) {
            this.set(__progress);
            return this.getLerp();
        },

        /**
         * retrieves current position of time or progression
         * @param   {Boolean} __time flag, if true - returns time progression value, else progression
         * @returns {Number}  time or progression value between 0.0 - 1.0
         */
        getLerp: function(__time) {
            //progression or time
            return __time ? this.point[0] : this.point[1];
        },

        /**
         * retrieves current position of time
         * @param   {Boolean} __time flag, if true - returns time progression value, else progression
         * @returns {Number}  time or progression value between 0.0 - 1.0
         */
        getLerpedTime: function() {
            //progression or time
            return this.point[0];
        },


        /**
         * retrieves current position of  progression
         * @param   {Boolean} __time flag, if true - returns time progression value, else progression
         * @returns {Number}  time or progression value between 0.0 - 1.0
         */
        getLerpedProgression: function() {
            //progression or time
            return this.point[1];
        },

        /**
         * returns current progress of the curve (different from progression and time dilation)
         * @returns {Number} current progress between 0.0 - 1.0
         */
        getProgress: function() {
            return this.__progress__;
        },

        /**
         * returns current duration value
         * @returns {Number} current duration in MS
         */
        getDuration: function() {
            return this.__duration__;
        }
    };

    //created class - sub-classing event class
    Bezier = $eventful.emitter.subclass('Bezier', Bezier);
    return Bezier;
});

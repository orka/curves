define(['p!-eventful', 'p!-assertful'], function($eventful, $assertful) {
    'use strict';
    var edge0 = 0.0;
    var edge1 = 1.0;
    var duration = 1000; // one sec default duration
    var Bezier;
    /*
    point [time, lerp]
    | lerp
    |
    |
    |
    |
    |
    ---------------------
                    time
     */
    /**
     * Interpolate values of A, B points in time 0-1
     * @param   {Array} __pointA time, lerped time values
     * @param   {Array} __pointB time, lerped time values
     * @param   {Number} __t      progress to interpolate
     * @returns {Array}          interpolated value of time, lerped time
     */
    function lerp(__pointA, __pointB, __t) {
        var _a1 = __pointA[0],
            _a2 = __pointA[1],
            _b1 = __pointB[0],
            _b2 = __pointB[1];
        return new Float32Array([(_b1 - _a1) * __t + _a1, (_b2 - _a2) * __t + _a2]);
    }

    /**
     * @class Bezier processory ot time, lerped time values by a duration in ms
     * as a subclass produces 'ended' event
     * @type {Bezier}
     */
    Bezier = {
        /**
         * @constructor
         * @param   {Array} __points   collection of controll points only - no limit
         * @param   {Number} __duration time in MS of a duration of the curve
         * @param {Boolean} __debug if true debug points will be collected this.__debugPoints
         */
        constructor: function(__points, __duration, __debug) {
            if (!$assertful.optArray(__points) || !$assertful.optPint(__duration)) {
                throw 'Invalid arguments';
            }
            //init events
            Bezier.super.constructor.call(this);

            if (__points) {
                //concatinate edge around points
                this.__points__ = [
                    new Float32Array([edge0, edge0])
                ].concat(__points).concat([
                    new Float32Array([edge1, edge1])
                ]);
            } else {
                this.__points__ = [
                    new Float32Array([edge0, edge0]), //time, lerped time
                    new Float32Array([edge1, edge1]) //time, lerped time
                ]; //[[#,#], ...]
            }
            //set duration
            this.__duration__ = __duration || duration;
            //start point in case value is retrieved before the update is called
            this.point = new Float32Array([0.0, 0.0]);
            //set start time
            this.__created__ = Date.now();
            //progress
            this.__progress__ = 0;

            if (__debug) {
                this.__debugPoints = [];
            }
        },

        /**
         * deconstructs self and super
         */
        deconstructor: function() {
            Bezier.super.deconstructor.call(this);
            this.__points__ = null;
            this.__duration__ = null;
            this.__created__ = null;
        },

        /**
         * resets start time and updates currentl point
         * "started" event is emitted
         */
        start: function() {
            this.__created__ = Date.now();
            this.update();
            this.emit('started');
        },

        /**
         * Sets point to end values
         * "ended" event is emitted
         */
        end: function() {
            this.__progress__ = 1.0;
            if (!this.ended) {
                this.ended = true;
                //do a final run
                this.processPoints(this.__points__, this.__progress__);
                //manually set point
                this.point = new Float32Array([1.0, 1.0]);
            } else {
                this.emit('ended');
            }
        },

        /**
         * Lerps points over time (now)
         * Starts progress if not started
         */
        update: function() {
            this.curve((Date.now() - this.__created__) / this.__duration__);
        },

        /**
         * Reduces points to one position time, lerped time that will be the current position point
         * @param   {Number} __progress 0-1 progresstion of curve
         */
        curve: function(__progress) {
            this.__progress__ = __progress;
            if (this.__debugPoints) {
                //reset array
                this.__debugPoints.length = 0;
            }
            //cap near time value (not lerp!)
            if (__progress > 0.9999) {
                this.end();
            } else {
                //osition will reflect the time/progress coordinates of the point calculus
                //it may be the the position progres is below or above the progress - depending on points coord
                this.point = this.processPoints(this.__points__, __progress);
            }
        },

        /**
         * Returns current progress of .point
         * @param   {Number} __progress Opt - override progress
         */
        set: function(__progress) {
            if (__progress) {
                this.curve(__progress);
            } else if (!this.__created__) {
                this.start();
            }
        },

        /**
         * retrieves corrent position of time and progression
         * @param   {Boolean} __time flag, if true - returns time progression value, else progression
         * @returns {Number}  time or progression value between 0.0 - 1.0
         */
        getLerp: function(__time) {
            //progression or time
            return __time ? this.point[0] : this.point[1];
        },

        /**
         * returns current progress of the curve (different from progression anf time dialation)
         * @returns {Number} current progress between 0.0 - 1.0
         */
        getProgress: function() {
            return this.__progress__;
        },

        /**
         * returns current duration value
         * @returns {Number} current duration in MS
         */
        getDuration: function() {
            return this.__duration__;
        },

        /**
         * Point array processing involves reduction of two points to one time, process values point or a position
         * Function will recurce till there is only one point remaining representing time, lerped time values
         * @param   {Array} __points   all points array (including edge)
         * @param   {Float} __progress 0-1 progress value by which to interpolate the points
         * @returns {Array}            time, lerped time value [#,#]
         */
        processPoints: function(__points, __progress) {
            var _i, _positions = [];

            //calculata all middle points relations
            for (_i = 0; _i < __points.length - 1; _i += 1) {
                //calculate the position in time, lerped time for each middle point
                //collect the result
                _positions.push(lerp(__points[_i], __points[_i + 1], __progress));
            }

            if (this.__debugPoints != null) {
                this.__debugPoints.push(_positions);
            }

            //if we have more than 2 positions - scan again
            if (_positions.length > 1) {
                return this.processPoints(_positions, __progress);
            }
            //return on final point
            return _positions[0];
        }
    };

    Bezier = $eventful.subclass('Bezier', Bezier);
    return Bezier;
});

define(
    [
        'p!-curves/bezier',
        'p!-curves/error'
    ],
    function($bezier, $error) {
        'use strict';
        return {
            bezier: $bezier,
            error: $error
        };
    }
);
